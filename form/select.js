export default class Select {
    constructor(className, doctorFirst, doctorSecond, doctorThird,) {
        this.className = className;
        this.doctorFirst = doctorFirst;
        this.doctorSecond = doctorSecond;
        this.doctorThird = doctorThird;
    }
    render(){
        const select = document.createElement('select');
        select.className = this.className;
        select.insertAdjacentHTML('beforeend', `<option>${this.doctorFirst}</option>,<option>${this.doctorSecond}</option>,<option>${this.doctorThird}</option>,` )
        return select
    }
}