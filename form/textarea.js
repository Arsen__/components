export default class Textarea {
    constructor(className, cols, rows) {
        this.className = className;
        this.cols = cols;
        this.rows = rows;
    }
    render(){
        const textarea = document.createElement('textarea');
        textarea.className = this.className;
        textarea.cols = this.cols;
        textarea.rows = this.rows;
        return textarea
    }
}