export default class Input {
    constructor(type, className, value) {
        this.type = type;
        this.className = className;
        this.value = value;
    }
    render(){
        const input = document.createElement('input');
        input.type = this.type;
        input.className = this.className;
        input.value = this.value;
        return input
    }
}